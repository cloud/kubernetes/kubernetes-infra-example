module "kubernetes_infra" {

  #  source = "./../../kubernetes-infra"
  source = "git::https://gitlab.ics.muni.cz/cloud/terraform/modules/kubernetes-infra.git?ref=v4.0.1"

  # Example of variable override
  ssh_public_key = "~/.ssh/id_rsa.pub"

  infra_name = "infra-name" 

  control_nodes_count       = 3
  control_nodes_volume_size = 30
  control_nodes_flavor      = "hpc.8core-16ram"

  worker_nodes = [
    {
      name        = "worker"
      flavor      = "standard.small"
      volume_size = 30
      count       = 2
    }
  ]
}
