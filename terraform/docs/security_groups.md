# Security group
The Kubernetes terraform infrastructure comes with default security group contains default rules.

You can modify or delete these default rules or add custom rules.

Default rule are: `alltcp, alludp, https4, http4, ssh4, icmp4, lb4`

## Override default rule

Add `custom_security_group_rules` to `main.tf` and add rule with the key of default rule (e.g. `http4`) and add custom values for the rule.

```
custom_security_group_rules = {
  http4 = {
    description      = "custom http"
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_range_min   = 8080
    port_range_max   = 8080
    remote_ip_prefix = "0.0.0.0/0"
  }
}
```

## Remove default rule
Add list of rules you want to remove to `main.tf`.

Fore example
```
remove_rules = ["ssh4","http4"]
```

## Add custom rule
Add `custom_security_group_rules` to `main.tf` and add rules:

```
custom_security_group_rules = {
  custom_ssh = {
    description      = "custom ssh"
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_range_min   = 2222
    port_range_max   = 2222
    remote_ip_prefix = "0.0.0.0/0"
  }
  custom_http = {
    description      = "custom http"
    direction        = "ingress"
    ethertype        = "IPv4"
    protocol         = "tcp"
    port_range_min   = 8080
    port_range_max   = 8080
    remote_ip_prefix = "0.0.0.0/0"
  }
}
```
