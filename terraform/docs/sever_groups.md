# Server groups


The Kubernetes terraform infrastructure allows you to create one or more server groups. The purpose of server groups is to be able to have more types and sizes of instances in the cluster with different flavors (for example GPU flavor).

More server groups can look like this:

```
worker_nodes = [
  {
      name        = "wg-blue"
      flavor      = "standard.small"
      volume_size = 30
      count       = 2
  },
  {
      name        = "wg-gpu"
      flavor      = "a3.32core-120ram"
      volume_size = 30
      count       = 2
  }
]
```
