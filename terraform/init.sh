#!/bin/bash
PROJECT_ID=$(openstack application credential show ${OS_APPLICATION_CREDENTIAL_ID} -f value -c project_id)
PROJECT_NAME=$(openstack project show ${PROJECT_ID} -f value -c name)
INFRA_NAME=$(grep -oP 'infra_name\s*=\s*"\K[^"]+' main.tf | awk '{print $1}')
CONTAINER_NAME="$PROJECT_NAME-$INFRA_NAME-tf-backend"
if openstack container show "$CONTAINER_NAME" >/dev/null 2>&1; then
    echo "Container already exists, skipping creation."
else
    # Container doesn't exist, create it
    openstack container create "$CONTAINER_NAME"
    echo "Container created."
fi
# Check if EC2 credentials already exist
existing_credentials=$(openstack ec2 credentials list -f value -c "Project ID" | grep ${PROJECT_ID} | grep -c '^')
if [ "$existing_credentials" -gt 0 ]; then
    access=$(openstack ec2 credentials list -f value | grep ${PROJECT_ID} |  awk '{print $1}')
    secret=$(openstack ec2 credentials list -f value | grep ${PROJECT_ID} |  awk '{print $2}')
    echo "EC2 credentials already exist, skipping creation."
else
    # Create new EC2 credentials
    CREDENTIALS=$(openstack ec2 credentials create -f shell)
    access=$(echo "$CREDENTIALS" | grep -o 'access="[^"]*"' | cut -d'"' -f2)
    secret=$(echo "$CREDENTIALS" | grep -o 'secret="[^"]*"' | cut -d'"' -f2)
    echo "EC2 credentials created."
fi    
cat > .tf-s3-creds << EOL
[default]
aws_access_key_id=${access}
aws_secret_access_key=${secret}
EOL
echo "Credential file created."
cat > backend.tf << EOL
 terraform { 
  backend "s3" {
    endpoints =                 { s3 = "$(openstack version show -c Endpoint -f value | grep object-store | sed 's/\/swift\/v1\///')"}
    shared_credentials_files    = ["./.tf-s3-creds"]
    bucket                      = "$CONTAINER_NAME"
    use_path_style              = true
    key                         = "terraform.tfstate"
    workspace_key_prefix        = "$(openstack container show $CONTAINER_NAME -c account -f value)"
    region                      = "$(openstack region list -c Region -f value)"
    skip_credentials_validation = true
    skip_region_validation      = true
    skip_requesting_account_id  = true
    skip_metadata_api_check     = true
    skip_s3_checksum            = true
  }
} 
EOL
echo "Backend file created."
