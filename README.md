## Prerequisites:
- terraform - [install](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- openstack-cli - [install](https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html)
- Form to access group project - https://projects.cloud.muni.cz/ (at least 1 Floating IP or more if you want use LoadBalancers, for master nodes we recommend to ask for `hpc.8core-16ram` )

## Create Infrastructure

Create openstack application credentials: 
 - Swich to project where you want to build the infrastructure 
 - On tab Identity > application credentials
 - \+ create application credential
 - Give a name > Create Applicaiton Credential
 - Download file! (The application credential secret will not be available after closing this page, so you must capture it now or download it. If you lose this secret, you must generate a new application credential)

```
git clone git@gitlab.ics.muni.cz:cloud/kubernetes/kubernetes-infra-example.git
```
If required you may change the values as needed. (like infrastructure name, cluster size (master nodes, worker nodes groups), etc.)
- see [docs](./terraform/docs/) for more information
- `~/kubernetes-infra-example/terraform/main.tf`
- Give a path to your public ssh key at: `ssh_public_key = "~/.ssh/id_rsa.pub"`

```
cd ~/kubernetes-infra-example/terraform/
source ~/path/to/credentials.sh
./init.sh
terraform init
terraform apply
```

## Create Kubernetes Cluster

#### Set parameters for the cluster

- In `../ansible/group_vars/` you can modify parameters for the cluster
- All possible group vars are in [Kubespray](https://github.com/kubernetes-sigs/kubespray) project.
- In `all/openstack.yml` modify the application credentials, which could be the same credentials as already created.


#### Install Kubernetes
Prerequisites: 
```
cd ../ansible/01-playbook/
sudo pip install -r requirements.txt
ansible-galaxy install -r requirements.yml
```

Run ansible playbook:
```
cd ../ansible/01-playbook/
ansible-playbook -i ../ansible_inventory --user=ubuntu --become --become-user=root play.yml
```

#### Access via config
- Kubeconfig is located in the artifacts directory, you can copy the config file to `~/.kube` directory for local access.

#### Access with SSH
- If you need to access the cluster
- Bastion ansible_hosts at `~/kubernetes-infra-example/ansible/ansible_inventory`
```
ssh -J ubuntu@<bastion_ip_address> ubuntu@<control-node_ip_address>
# For example:
# ssh -J ubuntu@195.113.167.169 ubuntu@10.10.10.26
sudo -i
kubectl get nodes
```
## Monitoring
To apply monitoring stack you need to switch <code>install_monitoring_task: true</code>, located at <code>[ansible/group_vars/all/all.yml](./ansible/group_vars/all/all.yml)</code>. It installs the <code><a href="https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack" target="_blank">kube-prometheus-stack</a></code> chart with custom values. Make sure to fill and modify those values at <code>[ansible/01-playbook/roles/monitoring/files/values.yaml](./ansible/01-playbook/roles/monitoring/files/values.yaml)</code>.
<br />

Tasks have its own <b>tags</b>, use these tags to update only the desired sections. For example update the values of the monitoring task, run:
```
ansible-playbook -i ../ansible_inventory --user=ubuntu --become --become-user=root play.yml --tags monitoring
```
### Access the services
```
kubectl get svc -n monitoring
kubectl port-forward svc/<service-name> <local_port>:<service_port> -n monitoring
# For example:
# kubectl port-forward svc/prometheus-operated 9090:9090 -n monitoring
# kubectl port-forward svc/prometheus-community-grafana  3000:80 -n monitoring
# Access the service in browser with <localhost>:<port>
```
For information on <b>persistent volume</b>, <b>alertmanager</b> and <b>grafana</b> configuration, refer to the [docs](./ansible/docs/).
## Cleanup
### Delete kubernetes cluster
```
cd ~/kubernetes-infra-example/terraform
terraform destroy
```
### Other resources purge
```
openstack container list #find bucket which is mentioned in backend.tf or bucket name is in format PROJECT_NAME-INFRA_NAME-tf-backend
openstack object list <container-name>
openstack object delete <container-name> <object-name> 
openstack container delete <container-name> 

openstack ec2 credentials list #find credentials which are mentioned in .tf-s3.creds
openstack ec2 credentials delete <access key>
```
