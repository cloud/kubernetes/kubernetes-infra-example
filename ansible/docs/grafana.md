### Grafana
- In the default monitoring settings change the adminPassword. 
- The MacOS dashboard is disabled by default. To enable it, set <code>nodeExporter.operatingSystems.darwin.enabled: True</code>. 
- The default configuration uses default grafana login. To use only the default grafana login without external access comment out the <code>ingress</code>, <code>assertNoLeakedSecrets</code> and <code>grafana.ini</code> sections under the <code>grafana</code> configuration.
#### Ingress
To manage external access to Grafana, you need to switch <code>install_nginx_task: true</code> to install the [ingress controller](https://github.com/kubernetes/ingress-nginx) with helm chart. Ensure that you have your own external domain address and specify it under <code>grafana.ingress.hosts</code> in the monitoring values. Check if the ingress has been succesfully created:
```
kubectl get ingress -n monitoring
```
#### Cert-manager
To add the HTTPS support switch <code>install_cert_manager_task: true</code>, which installs the [cert-manager](https://github.com/cert-manager/cert-manager/tree/master) helm chart and applies the <code>prod_issuer.yaml</code> manifest. Provide your domain address at <code>grafana.ingress.tls.hosts</code> and into the manifest specify your e-mail address. Check if the certificate has been succesfully created:
```
kubectl get certificate -n monitoring
```
#### Perun OAuth
To enable OAuth authentication with Perun, first ensure that <b>Ingress</b> is set up and preferably configure HTTPS with <b>Cert-manager</b>. Also, enable OAuth authentication by setting <code>auth.generic_oauth.enabled: true</code>. To register the service, fill out this [form](https://spadmin.e-infra.cz/auth/requests/new).  For details on connecting the service, refer to the [docs](https://perunaai.atlassian.net/wiki/spaces/EINFRACZ/pages/3932161/Connecting+the+service).
```
Important fields of the form:
- Choose OIDC as authentication method
Page [1]:
- URL of login page: https://<grafana.domain.cz>/login # input your domain address
Page [3]:
- Redirect URIs: https://<grafana.domain.cz>/login/generic_oauth # input your domain address
- Flow the service will use: authorization code, token exchange
- Token endpoint authentication method: client_secret_basic
- Proof Key for Code Exchange (PKCE) Code Challenge Method: SHA256 code challenge
- Scopes the service will use: openid, profile, email
```
Once the request is accepted, you should see the client id and secret at [My services](https://spadmin.e-infra.cz/auth/facilities/myServices) under the SAML/OIDC tab, input these into the monitoring config at <code>.auth.generic_oauth</code> section. Additionally, set the <code>.server.root_url</code> to your domain address, including the protocol.