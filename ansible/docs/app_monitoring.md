# Monitor your application

To monitor your applicaiton with Prometheus, we use the serviceMonitor.

## Step 1: Ensure Your Application Exposes Metrics
This is typically done through an endpoint that provides metrics in a format that Prometheus can scrape. Refer to yours application documentation for details on how to enable and access these metrics.

Example on exposing metrics for NGINX, using the default values of the NGINX helm chart.
```
...
controller:
  metrics:
    enabled: true # switched to 'true' to expose metrics
    service:
      annotations:
        prometheus.io/scrape: "true" # Tells prometheus to scrape this service for metrics
        prometheus.io/port: "10254" # Specifies the port on which metrics are exposed
...
```
## Step 2: Create a serviceMonitor for your application
To get the metrics endpoint, the endpoint is typically named <code>\<your-app>-metrics</code>, search available services in your application's namespace:
```
$ kubectl get svc -n <your-app-namespace>
NAME                                               TYPE           CLUSTER-IP      EXTERNAL-IP       PORT(S)                      
<your-app-service>-metrics     ClusterIP      10.233.41.104   <none>            10254/TCP                    
```
To obtain application labels, run:
```
$ kubectl describe svc <your-app-service>-metrics -n <your-app-namespace>
```
Create serviceMonitor.yaml resource: 
```
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: <your-app>-metrics
  namespace: monitoring
  labels:
    release: prometheus-community
    app: <your-app>
spec:
  endpoints:
    - port: metrics
      interval: 30s
      path: /metrics
  namespaceSelector:
    matchNames:
      - <your-app-namespace>
  selector:
    matchLabels:
      app.kubernetes.io/name: <your-app>
      app.kubernetes.io/component: <your-component>
```
Apply the serviceMonitor
```
$ kubectl apply -f ~/path/to/servicemonitor.yaml -n monitoring
List the service monitors:
$ kubectl get servicemonitors.monitoring.coreos.com -n monitoring
```
## Step 3: Register the ServiceMonitor with Prometheus
Add your <code>ServiceMonitor</code> to the monitoring <code>values.yaml</code> configuration:
```
prometheus:
  prometheusSpec:
    additionalServiceMonitors:
      - name: <your-app>-metrics
        namespace: monitoring
        selector:
          matchLabels:
            app.kubernetes.io/name: <your-app>
            app.kubernetes.io/component: <your-component>
        namespaceSelector:
          matchNames:
            - <your-app-namespace>
        endpoints:
          - port: metrics
            path: /metrics
            interval: 30s
```
## Step 4: Verify Monitoring
After deploying your <code>ServiceMonitor</code>, go to the <b>Status/Targets</b> tab in the Prometheus UI. You should see the target corresponding to your application. If the target is marked as <code>up</code>, then metrics are being scraped successfully.
- If you don’t see your target, verify that the <code>ServiceMonitor</code> labels match your application's service labels.
  - To further diagnose, connect to the master node via SSH and run: 
  ```
  # To get the apps IP address, run:
  $ kubectl describe pod <your-app-pod> -n <your-app-namespace>
  
  # To get the apps metrics port:
  $ kubectl get service -n <your-app-namespace>
  # The service will list the ports being used. Look for the port associated with metrics (usually labeled 'metrics' or similar).

  # Then, use curl to check if the metrics endpoint is working:
  $ curl http://<app-ip>:<metrics-port>/metrics
  ``` 
  - If the metrics are being exposed correctly, you should see output such as: 
  ```
  # HELP http_requests_total Total number of HTTP requests
  # TYPE http_requests_total counter
  http_requests_total{method="get",status="200"} 1024</code>
  ```
  - This indicates the issue may lie with the labels.
  - If no output or an error occurs, there might be a problem with the application's configuration or the metrics endpoint. Check the application's documentation for further troubleshooting.
- You can also query the metrics using PromQL to confirm they are being collected.