### Persistent volume and retention
Ansible installs the <code>storageClass</code> task that is used for volume claim <code>prometheus.prometheusSpec.storageSpec.volumeClaimTemplate</code>. The values of the volume claim like storage size or retention time and size can be modified in monitoring values file under the <code>prometheus</code> section.<br />

You can also increase the storage size after it has been already applied. The steps are outlined below:
```
kubectl get pvc -n monitoring
kubectl edit pvc -n monitoring <pvc-name>
```
Change the size located at <code>.spec.resources.requests.storage</code>. When using the vi/vim editor press <code>Insert</code> and change the value. After changing the value press <code>Esc</code> and type <code>:wq</code> and confirm it. Wait for changes to take effect. <br />
Check if the changes were succesfully applied with:
```
kubectl get pvc -n monitoring
```