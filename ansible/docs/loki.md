# Loki

To deploy Loki-stack, set <code>install_loki_task: true</code>.

## Storage

Using OpenStack UI, navigate to **Object Storage > Containers** and create a new container.

Or, using CLI, run the following command to create a new container:
```
openstack container create <container-name>
```

Run the following commands to create EC2 credentials for accessing the S3 storage:
```
CREDENTIALS=$(openstack ec2 credentials create -f shell)
access=$(echo "$CREDENTIALS" | grep -o 'access="[^"]*"' | cut -d'"' -f2)
secret=$(echo "$CREDENTIALS" | grep -o 'secret="[^"]*"' | cut -d'"' -f2)
echo "EC2 credentials created."
```
Retrieve the Access and Secret Keys:
```
echo "Access Key: $access"
echo "Secret Key: $secret"
```
Add the credentials to the <code>loki/values.yaml<code>.

Validate s3 storage:    
```
aws configure # Configure with generated access and secret keys
aws s3 ls s3://loki-logs/ --endpoint-url=https://object-store.cloud.muni.cz
aws s3 ls s3://loki-logs/index --endpoint-url=https://object-store.cloud.muni.cz --recursive
aws s3 ls s3://loki-logs/fake --endpoint-url=https://object-store.cloud.muni.cz --recursive
```